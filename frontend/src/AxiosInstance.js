import Axios from 'axios';
import PubSub from 'pubsub-js'

export const axios = Axios.create({
	baseURL: process.env.REACT_APP_BASE_URL
})

axios.defaults.headers.common['Authorization'] = localStorage.getItem('jwt') || "";

axios.interceptors.response.use(function (response) {
	PubSub.publish('RESPONSE_INTERCEPTED', response.data);
	return response;
}, function (error) {
	return Promise.reject(error);
});
