import React from "react";
import './DatabaseManager.css';
import {axios} from '../../AxiosInstance.js'
import DateRangePicker from '../DateRangePicker/DateRangePicker.js'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { format, parse } from 'date-fns'
import { save } from 'save-file' // https://www.npmjs.com/package/save-file


export default class DatabaseManager extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			status:null,
			deleteDateRange: null,
			downloadDateRange: null,
			showDeleteConfirm: false,
			isDownloading: false,
			isUploading: false
		}
		this.changeDownloadDateRange = this.changeDownloadDateRange.bind(this)
		this.changeDeleteDateRange = this.changeDeleteDateRange.bind(this)
		this.uploadTweets = this.uploadTweets.bind(this)
	}

	componentDidMount(){
		this.updateStatus()
	}

	changeDownloadDateRange(newRange){
		this.setState({downloadDateRange:newRange})
	}

	changeDeleteDateRange(newRange){
		this.setState({deleteDateRange:newRange})
	}

	updateStatus(){
		axios.get("/database/stats").then(resp=>{
			if(resp.data.success){
				this.setState({status:resp.data.data})
			}
		})
	}

	downloadTweets(){
		this.setState({isDownloading:true})
		var start = format(this.state.downloadDateRange.startDate,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
		var end = format(this.state.downloadDateRange.endDate,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
		var url = "/database/download?start="+start+"&end="+end
		axios.request({
			url:url,
			method:'GET',
			responseType: 'blob',
		}).then((response) => {
			save(new Blob([response.data],{type:response.data.type}), 'tweets.gz');
			this.setState({isDownloading:false})
		})
	}

	deleteTweets(){
		var start = format(this.state.deleteDateRange.startDate,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
		var end = format(this.state.deleteDateRange.endDate,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
		var url = "/database/clear?start="+start+"&end="+end
		axios.delete(url).then(resp => {
			if (resp.data.success){
				this.setState({showDeleteConfirm:false})
				this.updateStatus()
			}
		})
	}

	uploadTweets(event){
		var file = event.target.files[0]
		var formData = new FormData();
		formData.append("tweets", file);
		this.setState({isUploading:true})
		axios.post('/database/import', formData, {headers: {'Content-Type': 'multipart/form-data'}}).then(resp=>{
			this.setState({isUploading:false})
			this.updateStatus()
		})
	}

	renderStatus(){
		var statusContainer = {
			display: 'grid',
			gridTemplateColumns: '50% 50%'
		}
		if (this.state.status){
			var oldest = "---"
			if(this.state.status.oldest_tweet)
				oldest = format(parse(this.state.status.oldest_tweet, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", new Date()), "dd/MM/yyyy HH:mm:ss")
			var newest = "---"
			if(this.state.status.newest_tweet)
				newest = format(parse(this.state.status.newest_tweet, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", new Date()), "dd/MM/yyyy HH:mm:ss")
			return <div style={statusContainer}>
				<div>Total Tweets:</div><div>{this.state.status.total_tweets}</div>
				<div>Oldest Tweet:</div><div>{oldest}</div>
				<div>Newest Tweet:</div><div>{newest}</div>
				<div>Data Size:</div><div>{this.state.status.data_size.toFixed(2)} MB</div>
			</div>
		}
		return <React.Fragment />
	}

	renderDownloadTweets(){
		var downloadButton = <Button onClick={()=>{this.downloadTweets()}}
								color='primary'
								variant='contained'
								disableElevation={true}
								style={{marginTop:'15px', width:'100%'}}>
								Download
							</Button>
		if (this.state.isDownloading){
			downloadButton = <div style={{marginTop:'15px', textAlign:'center'}}><CircularProgress /></div>
		}
		return <div>
			<DateRangePicker onDateRangeChange={this.changeDownloadDateRange} />
			{downloadButton}
		</div>
	}

	renderDeleteTweets(){
		var start = format(new Date(),"dd/MM/yyyy")
		var end = format(new Date(),"dd/MM/yyyy")
		if(this.state.deleteDateRange){
			var start = format(this.state.deleteDateRange.startDate,"dd/MM/yyyy")
			var end = format(this.state.deleteDateRange.endDate,"dd/MM/yyyy")
		}
		var dialog = <Dialog
				        open={this.state.showDeleteConfirm}
				        onClose={()=>{this.setState({showDeleteConfirm:false})}}
				      >
						<DialogTitle id="alert-dialog-title">You are about to delete part of your data</DialogTitle>
						<DialogContent>
							<DialogContentText>
							You are deleting all tweets from {start} to {end}, are you sure you want to continue?
							</DialogContentText>
						</DialogContent>
						<DialogActions>
							<Button onClick={()=>{this.setState({showDeleteConfirm:false})}} color="primary">
								Cancel
							</Button>
							<Button onClick={()=>{this.deleteTweets()}} color="primary" autoFocus>
								Continue
							</Button>
						</DialogActions>
					</Dialog>
		return <div>
			{dialog}
			<DateRangePicker onDateRangeChange={this.changeDeleteDateRange} />
			<Button onClick={()=>{this.setState({showDeleteConfirm:true})}}
				color='secondary'
				variant='contained'
				disableElevation={true}
				style={{marginTop:'15px', width:'100%'}}>
				Delete
			</Button>
		</div>
	}

	renderUploadTweets(){
		if (this.state.isUploading)
			return <div style={{marginTop:'15px', textAlign:'center'}}><CircularProgress /></div>

		return <div>
				<Button
					fullWidth={true}
					variant="contained"
					component="label"
					color="primary"
					disableElevation={true}>
						Select File
						<input type="file" hidden onChange={this.uploadTweets} />
				</Button>
		</div>
	}

	render() {
		return (
			<div className="db-manager-container">
				<div className="db-section">
					<h1>General Stats</h1>
					<section>
						{this.renderStatus()}
					</section>
				</div>
				<div className="db-section">
					<h1>Download Tweets</h1>
					<section>
						{this.renderDownloadTweets()}
					</section>
				</div>
				<div className="db-section">
					<h1>Delete Tweets</h1>
					<section>
						{this.renderDeleteTweets()}
					</section>
				</div>
				<div className="db-section">
					<h1>Upload Tweets</h1>
					<section>
						{this.renderUploadTweets()}
					</section>
				</div>
			</div>
		);
	}
}