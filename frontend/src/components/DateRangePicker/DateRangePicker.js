import React from "react";
import './DateRangePicker.css';
import {axios} from '../../AxiosInstance.js'

import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

export default class DateRangePicker extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			startDate: new Date().setHours(0, 0, 0, 0),
			endDate: new Date().setHours(23, 59, 59, 0)
		}
		this.updateStart = this.updateStart.bind(this)
		this.updateEnd = this.updateEnd.bind(this)
	}

	componentDidMount(){
		this.props.onDateRangeChange({startDate:this.state.startDate, endDate: this.state.endDate})
	}

	updateStart(dateObj, dateStr){
		dateObj.setHours(0, 0, 0, 0)
		this.setState({startDate:dateObj}, async ()=>{
			this.props.onDateRangeChange({startDate:this.state.startDate, endDate: this.state.endDate})
		})
	}
	
	updateEnd(dateObj, dateStr){
		dateObj.setHours(23, 59, 59, 0)
		this.setState({endDate:dateObj}, async ()=>{
			this.props.onDateRangeChange({startDate:this.state.startDate, endDate: this.state.endDate})
		})
	}

	render() {
		return (
			<div className="daterange-container">
				<MuiPickersUtilsProvider utils={DateFnsUtils}>
					<div style={{display: 'flex'}}>
						<div style={{}}>
							<KeyboardDatePicker
								disableToolbar
								id="date-picker-inline"
								variant="inline"
								format="dd/MM/yyyy"
								label="Starting Date"
								value={this.state.startDate}
								onChange={this.updateStart}
								KeyboardButtonProps={{'aria-label': 'change start date'}}
								/>
						</div>
						<div style={{}}>
							<KeyboardDatePicker
								disableToolbar
								id="date-picker-dialog"
								variant="inline"
								format="dd/MM/yyyy"
								label="Ending Date"
								value={this.state.endDate}
								onChange={this.updateEnd}
								KeyboardButtonProps={{'aria-label': 'change end date',}}
								/>
						</div>
					</div>
				</MuiPickersUtilsProvider>
			</div>
		);
	}
}