import React from "react";
import './DaemonController.css';
import {axios} from '../../AxiosInstance.js'
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';

export default class DaemonController extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			isDaemonRunning:false,
			daemonStatus:''
		}
	}

	componentDidMount(){
		console.log("mounted")
		axios.get("/daemon/status").then(resp => {
			this.setState({
				isDaemonRunning:resp.data.data != "stopped",
				daemonStatus: resp.data.data
			})
		})
	}

	toggleDaemon(){
		var shouldRun = !this.state.isDaemonRunning
		var url = "/daemon/".concat(shouldRun?"start":"stop")
		console.log(url)
		axios.post(url).then(resp => {
			if (resp.data.success){
				this.setState({isDaemonRunning:shouldRun})
			}
		})

	}

	render() {
		if (this.state.daemonStatus == 'no_quota'){
			return <div className="no-quota">Usage cap exceeded</div>
		}
		return (
			<FormControlLabel
				label="Daemon"
				labelPlacement="end"
				control={<Switch checked={this.state.isDaemonRunning} onClick={()=>{this.toggleDaemon()}}/>}
			/>


		);
	}
}