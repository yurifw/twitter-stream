import React from "react";
import './PieChart.css';
import { ResponsivePie } from '@nivo/pie'  //https://nivo.rocks/
import { format, parse } from 'date-fns'
import _ from "lodash";
import IconButton from '@material-ui/core/IconButton';
import HorizontalSplitIcon from '@material-ui/icons/HorizontalSplit';
import BarChartIcon from '@material-ui/icons/BarChart';
import ToggleButton from '@material-ui/lab/ToggleButton';
import Button from '@material-ui/core/Button';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import SaveIcon from '@material-ui/icons/Save';
import {axios} from '../../AxiosInstance.js'
import ChartComponent from '../ChartComponent/ChartComponent.js';

export default class PieChart extends ChartComponent {
	constructor(props){
		super(props)
		this.state = {
			...this.state
		}
	}

	fetchData(){
		if(!this.props.dateRange) return null
		this.setState({loading:true})
		var start = format(this.props.dateRange.startDate,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
		var end = format(this.props.dateRange.endDate,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
		var url = this.props.endpoint+"?start="+start+"&end="+end
		url = url+"&source="+this.props.source
		axios.get(url).then(resp => {
			this.setState({
				chartData:resp.data.data,
				loading:false,
				noData: resp.data.data.graph_data.length == 0
			})
		})
	}

	renderControls(){
		return <React.Fragment />
	}

	renderChart(){
		return <ResponsivePie
	        data={this.state.chartData.graph_data}
	        padAngle={0.7}
	        colors={{ scheme: 'nivo' }}
			sliceLabel={(v)=>{return v.value+" - "+((v.value/this.state.chartData.total)*100).toFixed(2)+"%"}}
			legends={[{
	                dataFrom: 'keys',
	                anchor: 'right',
	                direction: 'column',
	                justify: false,
	                itemWidth: 150,
	                itemHeight: 20,
	                symbolSize: 20,
					translateY: -40,
	                itemDirection: 'left-to-right'
	            }]}
	    />
	}


}