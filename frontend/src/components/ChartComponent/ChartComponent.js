import React from 'react';
import './ChartComponent.css';
import CircularProgress from '@material-ui/core/CircularProgress';
import SaveIcon from '@material-ui/icons/Save';
import Button from '@material-ui/core/Button';

const saveSvgAsPng = require('save-svg-as-png') //save-svg-as-png
const imageOptions = {
	scale: 5,
	encoderOptions: 1,
	backgroundColor: 'white',
}


export default class ChartComponent extends React.Component {

	constructor(props){
		super(props)
		this.state = {
			loading: false,
			chartData: null
		}
		this.autoUpdate = this.autoUpdate.bind(this)

	}

	componentDidUpdate(prevProps) {
		if(this.props.dateRange != prevProps.dateRange || this.props.source != prevProps.source){
			this.fetchData()
		}
	}

	componentDidMount(){
		this.setState({
			intervalId:setInterval(this.autoUpdate, 3*1000)
		})
		this.fetchData()
	}

	componentWillUnmount(){
		clearInterval(this.state.intervalId);
	}

	saveChart(){
		var charts = document.getElementsByTagName("svg")
		for(var i =0; i<charts.length; i++ ){
			if(charts[i].classList.length == 0){
				saveSvgAsPng.saveSvgAsPng(charts[i], 'chart.png', imageOptions);
			}
		}
	}

	renderLoading(){
		return <div className="no-chart-container">
			<CircularProgress />
		</div>
	}

	renderNoData(){
		return <div className="no-chart-container">
			No data to display
		</div>
	}

	autoUpdate(){
		if (this.props && this.props.autoUpdate){
			this.fetchData()
		}
	}

	render() {
		if(this.state.chartData == null) return this.renderLoading()
		if(this.state.loading && !this.props.autoUpdate) return this.renderLoading()
		if(this.state.noData) return this.renderNoData()

		return <div className="chart-container">
			<div className="chart-controls">
				<div>
					<Button variant="outlined" onClick={()=>{this.saveChart()}}>
						<SaveIcon />
					</Button>
				</div>
				{this.renderControls()}
			</div>
			<div className="chart-wrapper">
				{this.renderChart()}
			</div>
		</div>
	}

}
