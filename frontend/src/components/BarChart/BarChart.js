import React from "react";
import './BarChart.css';
import { ResponsiveBar } from '@nivo/bar'  //https://nivo.rocks/
import { format, parse } from 'date-fns'
import _ from "lodash";
import IconButton from '@material-ui/core/IconButton';
import HorizontalSplitIcon from '@material-ui/icons/HorizontalSplit';
import BarChartIcon from '@material-ui/icons/BarChart';
import ToggleButton from '@material-ui/lab/ToggleButton';
import Button from '@material-ui/core/Button';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import {axios} from '../../AxiosInstance.js'
import ChartComponent from '../ChartComponent/ChartComponent.js';

export default class BarChart extends ChartComponent {
	constructor(props){
		super(props)
		this.state = {
			groupMode:"grouped",
			dateGroup:"day",
			chartData: null
		}
		this.autoUpdate = this.autoUpdate.bind(this)
	}

	fetchData(){
		if(!(this.props.dateRange)) return null
		this.setState({loading:true})
		var start = format(this.props.dateRange.startDate,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
		var end = format(this.props.dateRange.endDate,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
		var grouping = this.state.dateGroup
		var url = this.props.endpoint+"?start="+start+"&end="+end+"&grouping="+grouping
		url = url+"&source="+this.props.source
		axios.get(url).then(resp => {
			this.setState({
				chartData:resp.data.data,
				loading:false,
				noData: resp.data.data.length == 0
			})
		})
	}

	toggleGroupMode(){
		this.setState({groupMode:this.state.groupMode == "grouped"? "stacked" : "grouped"})
	}

	changeDateGroup(group){
		this.setState({dateGroup:group}, async () => {
			this.fetchData()
		})
	}

	renderControls(){
		var icon = this.state.groupMode == "grouped"?<HorizontalSplitIcon /> : <BarChartIcon />

		return <React.Fragment>
			<div>
				<Button variant="outlined" onClick={()=>{this.toggleGroupMode()}}>
					{icon}
				</Button>
			</div>
			<div>
				<ToggleButtonGroup
					value={this.state.dateGroup}
					exclusive
					onChange={(ev, gr)=>{this.changeDateGroup(gr)}}
					aria-label="text alignment"
					>
						<ToggleButton value="hour">Hour</ToggleButton>
						<ToggleButton value="day">Day</ToggleButton>
						<ToggleButton value="month"> Month </ToggleButton>
				</ToggleButtonGroup>
			</div>
		</React.Fragment>
	}

	renderChart(){
		if(!this.state.chartData)	return <div />

		var keys=[]
		this.state.chartData.forEach(bar =>{
			Object.keys(bar).forEach(key => {
				if (key == "date") return null
				if (keys.includes(key)) return null
				keys.push(key)
			});
		})

		const tickRenderer = ({ textAnchor, textBaseline, value, x, y }) => {
			return (
				<g transform={`translate(${x+20},${y+30})`}>
					<text alignmentBaseline={textBaseline} textAnchor={textAnchor} style={{transform:'rotate(30deg)', fontSize:'76%'}}>
						{value}
					</text>
				</g>
			);
		};

		return <ResponsiveBar
			data={this.state.chartData}
			keys={keys}
			indexBy="date"
			margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
			groupMode={this.state.groupMode}
			padding={0.3}
			colors={{ scheme: 'nivo' }}
			borderColor="inherit:darker(1.6)"
			axisTop={null}
			axisRight={null}
			axisBottom= {{
					tickSize: 8,
					tickPadding: 4,
					renderTick: tickRenderer
				}}
			label={d => ""}
			labelSkipWidth={12}
			labelSkipHeight={12}
			labelTextColor="inherit:darker(1.6)"
			animate={true}
			motionStiffness={90}
			motionDamping={15}
			legends={[{
	                dataFrom: 'keys',
	                anchor: 'top',
	                direction: 'row',
	                justify: false,
	                itemWidth: 150,
	                itemHeight: 20,
	                symbolSize: 20,
					translateY: -40,
	                itemDirection: 'left-to-right'
	            }]}
	   />
	}


}