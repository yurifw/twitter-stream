import React from "react";
import {axios} from '../../AxiosInstance.js'
import Tweet from 'react-tweet'  // https://github.com/mannynotfound/react-tweet
import './LastTweets.css';
import Switch from '@material-ui/core/Switch';
import MaterialSlider from '@material-ui/core/Slider';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { format, parse } from 'date-fns'
import AndroidRoundedIcon from '@material-ui/icons/AndroidRounded';
import LanguageRoundedIcon from '@material-ui/icons/LanguageRounded';
import PhoneIphoneRoundedIcon from '@material-ui/icons/PhoneIphoneRounded';
import TabletMacRoundedIcon from '@material-ui/icons/TabletMacRounded';
import DeviceUnknownRoundedIcon from '@material-ui/icons/DeviceUnknownRounded';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';  //https://github.com/express-labs/pure-react-carousel
import Button from '@material-ui/core/Button';

export default class LastTweets extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			tweets:[],
			isCarouselPlaying: true,
			tweetAmount: 5,
			source:["Twitter Web App","Twitter for Android","Twitter for iPhone","Twitter for iPad", "Unofficial"]
		}
		this.displayedTweets = []
		this.updateTweets = this.updateTweets.bind(this)
	}

	componentDidMount(){
		this.updateTweets()
	}

	updateTweets(){
		var url = "/twitter/last-tweets?amount="+(this.state.tweetAmount*1)
		url = url+"&source="+this.state.source
		axios.get(url).then(resp => {
			var newTweets = []
			this.setState({tweets:resp.data.data})
		})
	}

	renderIcon(source){
		var icon = <DeviceUnknownRoundedIcon />
		var label = source
		if(source == "Twitter Web App"){
			icon = <LanguageRoundedIcon />
			label = "Web App"
		}
		if(source == "Twitter for iPhone"){
			icon = <PhoneIphoneRoundedIcon />
			label = "iPhone"
		}
		if (source == "Twitter for Android"){
			icon = <AndroidRoundedIcon />
			label = "Android"
		}
		if (source == "Twitter for iPad"){
			icon = <TabletMacRoundedIcon />
			label = "iPad"
		}

		return <div>
			<div className="icon-container">
				{icon}
			</div>
			<div className="icon-label">
				{label}
			</div>
		</div>
	}

	renderTweet(tweet, index){
		const date = format(parse(tweet.data.created_at, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", new Date()), "EEE MMM dd HH:mm:ss '+0000' yyyy")
		const tweetData = {
			id_str: tweet._id,
			user: {
				name: tweet.includes.users[0].name,
				screen_name: tweet.includes.users[0].username,
				profile_image_url: 'https://cdn.cms-twdigitalassets.com/content/dam/help-twitter/twitter_logo_blue.png.twimg.768.png',
				followers_count: tweet.includes.users[0].public_metrics.followers_count
			},
			text: tweet.data.text,
			truncated:true,
			created_at: date,
			favorite_count: tweet.data.public_metrics.like_count,
			retweet_count: tweet.data.public_metrics.retweet_count,
			entities: null
		}
		const linkProps = {target: '_blank', rel: 'noreferrer'}
		return <Slide index={index} key={tweet._id}>
			<div className="tweet-line">
				<div className="tweet-wrapper">
					<Tweet data={tweetData} linkProps={linkProps} key={tweet._id}/>
				</div>
				<div className="source-wrapper">
					{this.renderIcon(tweet.data.source.parsed)}
				</div>
			</div>
		</Slide>
		// return  <div className="tweet-line" key={tweet._id}>
		// 	<div className="tweet-wrapper">
		// 		<Tweet data={tweetData} linkProps={linkProps} key={tweet._id}/>
		// 	</div>
		// 	<div className="source-wrapper">
		// 		{this.renderIcon(tweet.data.source.parsed)}
		// 	</div>
		//
		// </div>
	}

	renderControlBar(){
		return <div style={{display:'flex', flexDirection:'row'}}>
			<div style={{flex:1}}>
				<FormControlLabel
					style={{width:'100%'}}
					label="Sources"
					labelPlacement="top"
					control={
						<Select
							multiple={true}
							style={{width:'100%'}}
							value={this.state.source}
							onClose={(ev)=>{this.updateTweets()}}
							onChange={(ev)=>{this.setState({source:ev.target.value})}}
							>
							<MenuItem value={"Twitter for Android"}>Android</MenuItem>
							<MenuItem value={"Twitter for iPad"}>iPad</MenuItem>
							<MenuItem value={"Twitter for iPhone"}>iPhone</MenuItem>
							<MenuItem value={"Unofficial"}>Unofficial</MenuItem>
							<MenuItem value={"Twitter Web App"}>Web App</MenuItem>
						</Select>
					}
					/>
			</div>
			<div style={{flex:1}}>
				<FormControlLabel
					style={{width:'80%'}}
					label="Roll Tweets"
					labelPlacement="top"
					control={<Switch checked={this.state.isCarouselPlaying}
					onClick={()=>{this.setState({isCarouselPlaying:!this.state.isCarouselPlaying})}}/>}
					/>
			</div>
			<div style={{flex:1}}>
				<FormControlLabel
					style={{width:'80%'}}
					label="Tweets to show"
					labelPlacement="top"
					control={
						<MaterialSlider
							value={this.state.tweetAmount}
							onChangeCommitted={(ev, v)=>{this.updateTweets()}}
							onChange={(ev, v)=>{this.setState({tweetAmount:v})}}
							aria-labelledby="discrete-slider-small-steps"
							marks={null}
							min={1}
							max={100}
							valueLabelDisplay="auto"
							step={1}
						/>
					}
					/>
			</div>
			<div>
				<Button onClick={()=>{this.updateTweets()}}
					color='primary'
					variant='contained'
					disableElevation={true}
					style={{width:'100%'}}>
						Refresh Tweets
					</Button>
			</div>

		</div>
	}

	render() {
		return (
			<div className="tweets-container">
				<div className="control-bar">{this.renderControlBar()}</div>
				<CarouselProvider className="tweet-carousel"
					totalSlides={this.state.tweets.length}
					orientation="vertical"
					step={1}
					isPlaying={this.state.isCarouselPlaying}
					interval={1000}
					disableAnimation = {false}
					infinite={true}
				>
			        <Slider classNameTray="tweet-tray">
						{this.state.tweets.map( (t, i) => {return this.renderTweet(t, i)})}
					</Slider>
				</CarouselProvider>

			</div>
		);
	}
}