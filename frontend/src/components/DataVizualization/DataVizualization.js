import React from "react";
import './DataVizualization.css';
import {axios} from '../../AxiosInstance.js'
import { format, parse } from 'date-fns'
import DateRangePicker from '../DateRangePicker/DateRangePicker.js'
import BarChart from '../BarChart/BarChart.js'
import PieChart from '../PieChart/PieChart.js'
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';


export default class DataVizualization extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			dateRange:null,
			autoUpdateData: false,
			chartType: "Rules Bar Chart",
			source: ["Twitter Web App","Twitter for Android","Twitter for iPhone","Twitter for iPad", "Unofficial"]
		}
		this.updateDateRange = this.updateDateRange.bind(this)
	}

	updateDateRange(newRange){
		this.setState({dateRange:newRange})
	}

	toggleAutoUpdate(){
		this.setState({autoUpdateData:!this.state.autoUpdateData})
	}

	renderChart(){
		if(this.state.chartType == "Rules Bar Chart"){
			return <BarChart
				endpoint={"/vizualization/barchart/rules"}
				source={this.state.source}
				dateRange={this.state.dateRange}
				autoUpdate={this.state.autoUpdateData}
			/>
		}
		if(this.state.chartType == "Rules Pie Chart"){
			return <PieChart
				endpoint={"/vizualization/piechart/rules"}
				source={this.state.source}
				dateRange={this.state.dateRange}
				autoUpdate={this.state.autoUpdateData}
			/>
		}
		if(this.state.chartType == "Sources Bar Chart"){
			return <BarChart
				endpoint={"/vizualization/barchart/sources"}
				source={this.state.source}
				dateRange={this.state.dateRange}
				autoUpdate={this.state.autoUpdateData}
			/>
		}
		if(this.state.chartType == "Sources Pie Chart"){
			return <PieChart
				endpoint={"/vizualization/piechart/sources"}
				source={this.state.source}
				dateRange={this.state.dateRange}
				autoUpdate={this.state.autoUpdateData}
			/>
		}
	}

	render() {
		return (
			<div className="data-vizualization-container">
				<div className="data-form-controls">
					<div className="chart-type-control">
						<FormControl style={{width:'100%'}}>
							<InputLabel shrink id="demo-simple-select-placeholder-label-label">
								Chart Type
							</InputLabel>
							<Select
								value={this.state.chartType}
								onChange={(ev)=>{this.setState({chartType:ev.target.value})}}
								>
								<MenuItem value={"Rules Bar Chart"}>Rules Bar Chart</MenuItem>
								<MenuItem value={"Rules Pie Chart"}>Rules Pie Chart</MenuItem>
								<MenuItem value={"Sources Bar Chart"}>Sources Bar Chart</MenuItem>
								<MenuItem value={"Sources Pie Chart"}>Sources Pie Chart</MenuItem>
							</Select>

						</FormControl>

					</div>
					<div className="date-range-control">
						<DateRangePicker onDateRangeChange={this.updateDateRange}/>
					</div>
					<div className="source-control">
						<FormControl style={{width:'100%'}}>
							<InputLabel shrink id="demo-simple-select-placeholder-label-label">
								Source
							</InputLabel>
							<Select
								multiple={true}
								style={{width:'100%'}}
								value={this.state.source}
								onChange={(ev)=>{this.setState({source:ev.target.value})}}
								>
								<MenuItem value={"Twitter for Android"}>Android</MenuItem>
								<MenuItem value={"Twitter for iPad"}>iPad</MenuItem>
								<MenuItem value={"Twitter for iPhone"}>iPhone</MenuItem>
								<MenuItem value={"Unofficial"}>Unofficial</MenuItem>
								<MenuItem value={"Twitter Web App"}>Web App</MenuItem>
							</Select>

						</FormControl>

					</div>
					<div className="auto-update-control">
						<FormControlLabel
							label="Live Data"
							labelPlacement="top"
							control={<Switch checked={this.state.autoUpdateData} onClick={()=>{this.toggleAutoUpdate()}}/>}
						/>
					</div>
				</div>
				<div className="charts-container">
					{this.renderChart()}
				</div>

			</div>


		);
	}
}