import React from "react";
import './RuleManager.css';
import {axios} from '../../AxiosInstance.js'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Switch from '@material-ui/core/Switch';
import IconButton from '@material-ui/core/IconButton';
import HelpIcon from '@material-ui/icons/Help';

export default class RuleManager extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			tag:"",
			value:"",
			rules:[]
		}
		this.changeTag = this.changeTag.bind(this)
		this.changeValue = this.changeValue.bind(this)
	}

	componentDidMount(){
		this.reloadState()
	}

	reloadState(){
		axios.get("/rules").then(resp => {
			if (resp.data.success){
				this.setState({rules:resp.data.data, tag:"", value:""})
			}
		})
	}

	changeTag(event){
		this.setState({tag:event.target.value})
	}

	changeValue(event){
		this.setState({value:event.target.value})
	}

	addRule(){
		var payload = {
			tag:this.state.tag,
			value:this.state.value
		}
		axios.post("/rules", payload).then(resp => {
			if(resp.data.success){
				this.reloadState()
			}
		})
	}

	toggleRule(rule){
		console.log(rule)
		rule.active = !rule.active
		var url = "/rules/".concat(rule._id)
		if (rule.active){
			axios.post(url).then(resp => {if (resp.success) this.reloadState()})
		} else {
			axios.delete(url).then(resp => {if (resp.success) this.reloadState()})
		}
	}

	renderRules(rules){
		return <TableContainer component={Paper}>
				<Table aria-label="simple table">
					<TableHead>
						<TableRow>
							<TableCell>Value</TableCell>
							<TableCell align="right">Tag</TableCell>
							<TableCell align="right"></TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
					{rules.map((rule) => (
						<TableRow key={rule.id}>
							<TableCell component="th" scope="row">{rule.value}</TableCell>
							<TableCell align="right">{rule.tag}</TableCell>
							<TableCell align="right"><Switch checked={rule.active} onClick={()=>{this.toggleRule(rule)}} /></TableCell>
						</TableRow>
					))}
					</TableBody>
				</Table>
			</TableContainer>

	}

	render() {
		return (
			<div className="rules-container">
				<form className="rules-form">
				<div className="rules-form-field"><TextField label="Rule Value" onChange={this.changeValue} value={this.state.value}/></div>
					<div className="rules-form-field"><TextField label="Rule Tag/Name" onChange={this.changeTag} value={this.state.tag}/></div>
					<div className="rules-form-field">
						<Button onClick={()=>{this.addRule()}} color='primary' variant='contained' disableElevation={true}>
							Add Rule
						</Button>
					</div>
					<div>
						<IconButton target="_blank" href="https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/integrate/build-a-rule"><HelpIcon /></IconButton>
					</div>
				</form>
				<div className="current-rules-container">{this.renderRules(this.state.rules)}</div>
			</div>
		);
	}
}