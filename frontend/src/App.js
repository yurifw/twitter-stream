import React, {useState, useEffect} from 'react';
import PubSub from 'pubsub-js'
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";

import DaemonController from './components/DaemonController/DaemonController.js'
import DataVizualization from './components/DataVizualization/DataVizualization.js'
import RuleManager from './components/RuleManager/RuleManager.js'
import DatabaseManager from './components/DatabaseManager/DatabaseManager.js'
import LastTweets from './components/LastTweets/LastTweets.js'

import { Drawer } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { NavLink } from "react-router-dom";
import MenuItem from '@material-ui/core/MenuItem';

import TwitterIcon from '@material-ui/icons/Twitter';
import GavelIcon from '@material-ui/icons/Gavel';
import StorageIcon from '@material-ui/icons/Storage';
import BarChartIcon from '@material-ui/icons/BarChart';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function App() {
	const [drawerOpen, setDrawerOpen] = useState(false)
	const [showToast, setShowToast] = useState(false);
	const [toastMessage, setToastMessage] = useState("");
	const [toastSeverity, setToastSeverity] = useState("success");

	var responseNotifier = function (msg, data) {
		if(data.msg){
			setToastSeverity(data.success?"success":"error")
			setToastMessage(data.msg)
			setShowToast(true)
		}
	};
	useEffect(()=>{
		var token = PubSub.subscribe('RESPONSE_INTERCEPTED', responseNotifier);
	})

	return (

		<React.Fragment>
			<AppBar position="static">
				<Toolbar>
					<IconButton edge="start" color="inherit" aria-label="menu" onClick={()=>{setDrawerOpen(true)}}>
						<MenuIcon />
					</IconButton>

					<Typography variant="h6" style={{flexGrow:1}}>
						Twitter Live Stream
					</Typography>

					<IconButton edge="end" color="inherit" aria-label="menu">
						<DaemonController  />
					</IconButton>
				</Toolbar>
			</AppBar>

			<Router>
				<Drawer anchor='left' open={drawerOpen} onClose={()=>{setDrawerOpen(false)}}>
					<div className="menu-header">Twitter Filtered Stream</div>
					<MenuItem>
						<NavLink to="/live" className="menu-link" activeClassName="menu-link"><TwitterIcon /> Live Tweets</NavLink>
					</MenuItem>
					<MenuItem>
						<NavLink to="/rules" className="menu-link" activeClassName="menu-link"><GavelIcon /> Rules Management</NavLink>
					</MenuItem>
					<MenuItem>
						<NavLink to="/vizualization" className="menu-link" activeClassName="menu-link"><BarChartIcon /> Vizualization</NavLink>
					</MenuItem>
					<MenuItem>
						<NavLink to="/database" className="menu-link" activeClassName="menu-link"><StorageIcon /> Database</NavLink>
					</MenuItem>
				</Drawer>

				<Switch>
					<Route path="/live"><LastTweets /></Route>
					<Route path="/rules"><RuleManager /></Route>
					<Route path="/vizualization"><DataVizualization /></Route>
					<Route path="/database"><DatabaseManager /></Route>
					<Route path="/"><LastTweets /></Route>
				</Switch>
			</Router>

			<Snackbar open={showToast} autoHideDuration={5000} onClose={()=>{setShowToast(false)}}>
				<Alert onClose={()=>{setShowToast(false)}} severity={toastSeverity}>
					{toastMessage}
				</Alert>
			</Snackbar>

		</React.Fragment>
	);
}

export default App;
