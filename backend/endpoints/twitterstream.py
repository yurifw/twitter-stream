from flask import Flask, make_response, request
import json
import requests
import os
from . import TOKEN, CONFIG, DB
from .data_vizualization import args_2_aggregation

def start_daemon():
	os.remove("%sstopped" % CONFIG['daemon_directory'])
	open("%swaiting" % CONFIG['daemon_directory'], 'a').close()
	return make_response({"msg":"Daemon is starting, but may take up to a minute", "data":None, "success":True})

def stop_daemon():
	files = os.listdir(CONFIG['daemon_directory'])
	for f in files:
		os.remove("%s%s" % (CONFIG['daemon_directory'], f))
	open("%sstopped" % CONFIG['daemon_directory'], 'a').close()
	return make_response({"msg":"Daemon is stopped", "data":None, "success":True})

def daemon_status():
	files = os.listdir(CONFIG['daemon_directory'])
	return make_response({"msg":None, "data":files[0], "success":True})

def last_tweets():
	amount = int(request.args.get('amount',"5"))
	query = {}
	if request.args.get("source"):
		query['data.source.parsed'] = {"$in":request.args.get("source").split(",")}
	tweets = DB.tweets.find(query).sort([("data.created_at",-1)]).limit(amount)
	result = []
	for tweet in tweets:
		tweet['data']['created_at'] = tweet['data']['created_at'].strftime("%Y-%m-%dT%H:%M:%S.000Z")
		result.append(tweet)
	return make_response({"msg":None, "data":result, "success":True})



