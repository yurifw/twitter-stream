from flask import Flask, make_response, request
import json
from datetime import datetime
from . import DB
import pymongo
import time

def args_2_aggregation(args):
	aggregation = []
	if args.get('start'):
		aggregation.append({"$match":{"data.created_at":{
			"$gte": datetime.strptime(args['start'], "%Y-%m-%dT%H:%M:%S.000Z")
		}}})
	if args.get('end'):
		aggregation.append({"$match":{"data.created_at":{
			"$lt": datetime.strptime(args['end'], "%Y-%m-%dT%H:%M:%S.000Z")
		}}})
	if args.get('source'):
		aggregation.append(
			{"$match":
				{"data.source.parsed":{"$in":request.args.get("source").split(",")}}
			})
	if args.get('date_to_string'):
		aggregation.append({"$set":
			{"data.created_at":{
				"$dateToString": {"format":"%Y-%m-%dT%H:%M:%S.000Z","date":"$data.created_at"}
			}}
		})
	return aggregation

def rules_pie_chart():
	aggregation = args_2_aggregation(request.args)
	aggregation.extend([
		{"$unwind":"$matching_rules"},
		{"$project":{"matching_rules":1, "_id":0}},
		{"$group":{"_id":"$matching_rules.tag", "count":{"$sum":1}}},
		{"$sort": {"_id":1}}
	])
	cursor = DB.tweets.aggregate(aggregation)
	data = []
	total = 0
	for t in cursor:
		data.append({"value": t['count'], "label": t['_id'], "id": t['_id']})
		total = total + t['count']
	return make_response({"msg":None, "data":{"graph_data":data, "total":total}, "success":True})

def sources_pie_chart():
	aggregation = args_2_aggregation(request.args)
	aggregation.extend([
		{"$addFields":{"source":"$data.source.parsed"}},
		{"$group":{"_id":"$data.source.parsed", "count":{"$sum":1}}},
		{"$sort": {"_id":1}}
	])
	cursor = DB.tweets.aggregate(aggregation)
	data = []
	total = 0
	for t in cursor:
		data.append({"value": t['count'], "label": t['_id'], "id": t['_id']})
		total = total + t['count']
	return make_response({"msg":None, "data":{"graph_data":data, "total":total}, "success":True})


def rules_bar_chart():
	grouping = request.args.get("grouping", "day")
	format = "%d/%m/%Y %H:%M"
	if grouping == "hour":
		format = "%d/%m/%Y %H:00"
	if grouping == "day":
		format = "%d/%m/%Y"
	if grouping == "month":
		format = "%m/%Y"

	aggregation = args_2_aggregation(request.args)
	aggregation.extend([
		{"$set":
			{"data.created_at":{
				"$dateToString": {"format":format,"date":"$data.created_at"}
			}}
		},
		{"$unwind":"$matching_rules"},
		{"$group":{
			"_id":{"date":"$data.created_at","tag":"$matching_rules.tag"},
			"count":{"$sum":1}
		}},
		{"$group":{
			"_id":"$_id.date",
			"values":{"$push":{"tag":"$_id.tag","count":"$count"}}
		}},
		{"$sort": {"_id":1}}
	])
	cursor = DB.tweets.aggregate(aggregation)
	data = []
	for t in cursor:
		d = {"date":t['_id']}
		for rule in t['values']:
			d[rule['tag']] = rule['count']
		data.append(d)

	return make_response({"msg":None, "data":data, "success":True})

def sources_bar_chart():
	grouping = request.args.get("grouping", "day")
	format = "%d/%m/%Y %H:%M"
	if grouping == "hour":
		format = "%d/%m/%Y %H:00"
	if grouping == "day":
		format = "%d/%m/%Y"
	if grouping == "month":
		format = "%m/%Y"

	aggregation = args_2_aggregation(request.args)
	aggregation.extend([
		{"$set":
			{"data.created_at":{
				"$dateToString": {"format":format,"date":"$data.created_at"}
			}}
		},
		{"$unwind":"$matching_rules"},
		{"$group":{
			"_id":{"date":"$data.created_at","source":"$data.source.parsed"},
			"count":{"$sum":1}
		}},
		{"$group":{
			"_id":"$_id.date",
			"values":{"$push":{"source":"$_id.source","count":"$count"}}
		}},
		{"$sort": {"_id":1}}
	])
	cursor = DB.tweets.aggregate(aggregation)
	data = []
	for t in cursor:
		d = {"date":t['_id']}
		for rule in t['values']:
			d[rule['source']] = rule['count']
		data.append(d)

	return make_response({"msg":None, "data":data, "success":True})