from flask import Flask, make_response, request
import json
import requests
import os
from . import TOKEN, CONFIG, TWITTER_HEADERS, DB
import pymongo

def activate_rule(rule_id):
	rule = DB.rules.find_one({"_id":rule_id})
	rules = [rule]
	url = "https://api.twitter.com/2/tweets/search/stream/rules"
	resp = requests.post(url, headers=TWITTER_HEADERS, json={"add":rules})
	resp = json.loads(resp.text)
	id = resp['data'][0]['id']
	DB.rules.delete_one({"_id":rule['_id']})
	rule['_id'] = id
	DB.rules.insert_one(rule)
	return {"msg":"Rule is now active", "data":None, "success":True}


def deactivate_rule(rule_id):
	url = "https://api.twitter.com/2/tweets/search/stream/rules"
	payload = {"delete":{"ids":[rule_id]}}
	resp = requests.post(url, headers=TWITTER_HEADERS, json=payload)
	resp = json.loads(resp.text)
	success = resp['meta']['summary']['deleted'] == 1
	msg = "Rule deactivated" if success else "Unkown error"
	return {"msg":msg, "data":None, "success":success}

def add_rule():
	rule = {
		"value":request.json['value'],
		"tag": request.json['tag']
	}
	rules = [rule]

	url = "https://api.twitter.com/2/tweets/search/stream/rules"
	resp = requests.post(url, headers=TWITTER_HEADERS, json={"add":rules})
	resp = json.loads(resp.text)

	if 'errors' in resp:
		msg = "Twitter API Error: %s" % resp['errors'][0]['details'][0]
		return {"msg":msg, "data":None, "success":False}

	id = resp['data'][0]['id']
	rule['_id'] = id
	DB.rules.insert_one(rule)
	return {"msg":"New rule added", "data":None, "success":True}
	


def list_rules():
	url = "https://api.twitter.com/2/tweets/search/stream/rules"
	resp = requests.get(url, headers=TWITTER_HEADERS)
	active_rules = json.loads(resp.text)
	active_rules = active_rules.get('data',[])
	active_ids = [r['id'] for r in active_rules]
	rules = DB.rules.find().sort([("value",1)])
	result = []
	for rule in rules:
		rule['active'] = rule['_id'] in active_ids
		result.append(rule)
	return make_response({"msg":None, "data":result, "success":True})
