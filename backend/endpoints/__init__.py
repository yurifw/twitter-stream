import json
import os
import pymongo

print("reading config")
TOKEN=""
CONFIG={}

with open('../config.json') as config:
	CONFIG = json.loads(config.read())
	TOKEN = CONFIG['twitter_bearer_token']

TWITTER_HEADERS = {
	"Content-type": "application/json",
	"Authorization": "Bearer %s" % TOKEN
}

client = pymongo.MongoClient(CONFIG['mongo_connection_string'])
DB = client.twitterStream