from flask import Flask, make_response, request, send_file
import json
import requests
import os
from datetime import datetime
from . import TOKEN, CONFIG, TWITTER_HEADERS, DB
import pymongo
import re
import subprocess
import io
import zipfile
import time

def list_general_stats():
	total_tweets = DB.tweets.count()
	try:
		oldest = DB.tweets.find().sort([("data.created_at",1)]).limit(1)
		oldest = oldest[0]['data']['created_at'].strftime("%Y-%m-%dT%H:%M:%S.000Z")
	except IndexError:
		oldest = None
	try:
		newest = DB.tweets.find().sort([("data.created_at",-1)]).limit(1)
		newest = newest[0]['data']['created_at'].strftime("%Y-%m-%dT%H:%M:%S.000Z")
	except IndexError:
		newest = None
	size = DB.command("dbstats")
	size = size['dataSize'] / 1024 / 1024  # showing size in mega bytes
	stats={
		"total_tweets":total_tweets,
		"oldest_tweet":oldest,
		"newest_tweet":newest,
		"data_size": size
	}
	return make_response({"msg":None, "data":stats, "success":True})

def clear_tweets():
	start = request.args.get('start')
	start = datetime.strptime(start, "%Y-%m-%dT%H:%M:%S.000Z")
	end = request.args.get('end')
	end = datetime.strptime(end, "%Y-%m-%dT%H:%M:%S.000Z")
	cursor = DB.tweets.delete_many({"data.created_at":{
		"$gte": start,
        "$lt": end
	}})
	deleted = cursor.deleted_count
	msg = "Deleted %s Tweets" % deleted
	return make_response({"msg":msg, "data":None, "success":True})

def import_tweets():
	if 'tweets' not in request.files:
		return make_response({"msg":"Error: file not found in request", "data":None, "success":False})

	with zipfile.ZipFile(io.BytesIO(request.files['tweets'].read())) as zipped:
		file_name = zipped.infolist()[0].filename
		zipped.extract(file_name)

	export_cmd = """mongoimport -d twitterStream -c tweets --type=json --file %s --uri="%s" --mode=upsert """
	export_cmd = export_cmd % (file_name, CONFIG['mongo_connection_string'])
	out = subprocess.run([export_cmd], capture_output=True, shell=True)
	os.remove(file_name)
	return make_response({"msg":"All tweets imported", "data":None, "success":True})

def export_tweets():
	start = request.args['start']
	end = request.args['end']
	validator = "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}Z"
	validator = re.compile(validator)
	if validator.search(start) is None or validator.search(end) is None:
		return make_response({"msg":"Correct date format: %Y-%m-%dT%H:%M:%S.000Z", "data":None, "success":False})

	file_name = "%s_%s.json" % (start, end)
	export_cmd = """mongoexport -d twitterStream -c tweets --type=json --query='{"data.created_at":{"$gte":{"$date":"%s"},"$lt":{"$date":"%s"}}}' --out %s --uri="%s" """
	export_cmd = export_cmd % (
		start,
		end,
		file_name,
		CONFIG['mongo_connection_string'])
	subprocess.run([export_cmd], capture_output=True, shell=True)

	data = io.BytesIO()
	with zipfile.ZipFile(data, 'w', zipfile.ZIP_DEFLATED) as z:
		z.write(file_name)
	data.seek(0)
	os.remove(file_name)

	return send_file(data, attachment_filename='%s.gz' % file_name, as_attachment=True)
