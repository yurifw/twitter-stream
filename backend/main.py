# nohup gunicorn -w 4 --reload main:app > /root/mgc-scrapers/gunicorn.log &

import os
from flask import Flask, send_file
from flask import render_template
from flask_cors import CORS
from flask import request

import endpoints.twitterstream as ts
import endpoints.rules as rules
import endpoints.db_stats as db_stats
import endpoints.data_vizualization as data_vizualization

app = Flask(__name__, static_folder='public/', template_folder='public/')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.debug = True
CORS(app)

app.add_url_rule('/api/twitter/last-tweets', 'tsls', ts.last_tweets, methods=['GET'])
app.add_url_rule('/api/daemon/start', 'tsbd', ts.start_daemon, methods=['POST'])
app.add_url_rule('/api/daemon/stop', 'tsed', ts.stop_daemon, methods=['POST'])
app.add_url_rule('/api/daemon/status', 'tsds', ts.daemon_status, methods=['GET'])
app.add_url_rule('/api/rules', 'rar', rules.add_rule, methods=['POST'])
app.add_url_rule('/api/rules', 'rlr', rules.list_rules, methods=['GET'])
app.add_url_rule('/api/rules/<rule_id>', 'rdr', rules.deactivate_rule, methods=['DELETE'])
app.add_url_rule('/api/rules/<rule_id>', 'rac', rules.activate_rule, methods=['POST'])
app.add_url_rule('/api/database/stats', 'dbl', db_stats.list_general_stats, methods=['GET'])
app.add_url_rule('/api/database/clear', 'dbc', db_stats.clear_tweets, methods=['DELETE'])
app.add_url_rule('/api/database/import', 'dbi', db_stats.import_tweets, methods=['POST'])
app.add_url_rule('/api/database/download', 'dbdl', db_stats.export_tweets, methods=['GET'])
app.add_url_rule('/api/vizualization/piechart/rules', 'dvrpc', data_vizualization.rules_pie_chart, methods=['GET'])
app.add_url_rule('/api/vizualization/piechart/sources', 'dvspc', data_vizualization.sources_pie_chart, methods=['GET'])
app.add_url_rule('/api/vizualization/barchart/rules', 'dvrbc', data_vizualization.rules_bar_chart, methods=['GET'])
app.add_url_rule('/api/vizualization/barchart/sources', 'dvsbc', data_vizualization.sources_bar_chart, methods=['GET'])




@app.errorhandler(404)
def not_found(e):
	requested_file = request.path[1:]
	if len(request.path[1:]) == 0:
		return app.send_static_file("index.html")

	if '.' in requested_file:
		requested_file = '/'.join(request.path.split("/")[1:])
		return app.send_static_file(requested_file)
	else:
		print("serving index")
		return app.send_static_file("index.html")
