# python3 /root/atom-projects/twitter-stream/scripts/twitter_daemon.py /root/atom-projects/twitter-stream/config.json
import json
import requests
import sys
import os
from datetime import datetime
import pymongo

"""
This daemon should be invoked by a cronjob every minute.
The daemon checks for a status file in the directory provided
in the daemon_directory config.

A status file is an empty file in the directory, the file's name is the current
status of the daemon. There should be only one file in that directory,
otherwise unpredictable behavior may occur.

If no files are found, a file is created with the name of "stopped" and this is
the current status of the daemon.
The daemon can have 4 status: 'waiting', 'stopped', 'running', 'no_quota'
* waiting
	the next time the daemon is invoked it will be
	started and save the tweets to the db
* stopped
	the daemon will do nothing if invoked, the daemon also checks the status
	after each saved tweet. If the status is stopped, the daemon will stop
* running
	means the daemon is already running, if invoked again it will not open a
	tweet stream
* no_quota
	the monthly quota for the Filtered Stream API has ended, while in this status
	the daemon will try to save a single tweet every minute, if it suceeds it will
	change its status to stopped, if it fails it will keep the 'no_quota' status
"""


def log(s):
	"""prepends a timestamp to a string and print it to stdout"""
	ts = datetime.now().strftime("[%Y-%m-%d %H:%M:%S]")
	print("%s %s" % (ts, s))

config={}
with open(sys.argv[1]) as config:
	config = json.loads(config.read())

def check_status():
	"""check the daemon directory to get the daemon status"""
	files = os.listdir(config['daemon_directory'])
	if len(files)==0:
		open("%sstopped" % config['daemon_directory'], 'a').close()
		return "stopped"
	return files[0]

def parse_source(source):
	official = ["Twitter Web App", "Twitter for Android", "Twitter for iPad", "Twitter for iPhone"]
	return {
		"original": source,
		"parsed": source if source in official else "Unofficial"
	}

def set_no_quota_status():
	open("%sno_quota" % config['daemon_directory'], 'a').close()

def connect_stream(checking_quota=False):
	"""connects to the tweet stream and saves the data, to change fields check the twitter docs:
	https://developer.twitter.com/en/docs/twitter-api/fields
	"""
	client = pymongo.MongoClient(config['mongo_connection_string'])
	db = client.twitterStream

	params = {
		"tweet.fields":"public_metrics,created_at,source",
		"expansions":"author_id",
		"user.fields":"public_metrics"
	}
	headers = {
		"Content-type": "application/json",
		"Authorization": "Bearer %s" % config['twitter_bearer_token']
	}
	url = "https://api.twitter.com/2/tweets/search/stream"
	resp = requests.get(url, headers=headers, params=params, stream=True)

	for response_line in resp.iter_lines():
		if response_line:
			tweet = json.loads(response_line.decode())

			if 'data' not in tweet and tweet.get('title') == 'UsageCapExceeded':
				set_no_quota_status()
				log("no API quota, stopping daemon")
				exit()

			tweet['_id']=tweet['data']['id']
			tweet_ts = tweet['data']['created_at']
			tweet_ts = datetime.strptime(tweet_ts, "%Y-%m-%dT%H:%M:%S.000Z")
			tweet['data']['created_at'] = tweet_ts
			tweet['data']['source'] = parse_source(tweet['data']['source'])
			result = db.tweets.insert_one(tweet)
			log("twitter saved: %s" % result.inserted_id)

		if checking_quota:
			stop_daemon()

		if check_status() == "stopped":
			log("exit signal, stopping daemon")
			exit()




def start_daemon():
	os.remove("%swaiting" % config['daemon_directory'])
	open("%srunning" % config['daemon_directory'], 'a').close()
	connect_stream()

def stop_daemon():
	try:
		os.remove("%srunning" % config['daemon_directory'])
	except FileNotFoundError:
		pass
	if 'no_quota' not in os.listdir(config['daemon_directory']):
		open("%sstopped" % config['daemon_directory'], 'a').close()

if len(sys.argv) <2:
	log("error: no path for configuration file was provided, "\
		"the correct usage is: python3 twitter_daemon.py /root/config.json")
	exit()

if check_status() == "running":
	log("daemon already running, exiting")
	exit()

if check_status() == "no_quota":
	os.remove("%sno_quota" % config['daemon_directory'])
	connect_stream(True)
	exit()

if check_status() == "waiting":
	try:
		log("starting daemon")
		start_daemon()
	# except Exception as e:
	# 	log("unexpected error: %s" % str(e))
	finally:
		stop_daemon()

