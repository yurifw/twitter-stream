import pymongo

client = pymongo.MongoClient("mongodb://localhost:27017")
db = client.twitterStream
sources = db.tweets.distinct("data.source")
for source in sources:
	count = db.tweets.find({"data.source":source}).count()
	if(count > 100):
		print("%s - %s" % (source, count))