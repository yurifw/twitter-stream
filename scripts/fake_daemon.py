import random
import pymongo
import time
from datetime import datetime
import string

def random_text(min_len, max_len):
	letters = list(string.ascii_lowercase)
	letters.extend([" "]*10)
	return ''.join(random.choices(letters, k=random.randint(min_len, max_len)))

def get_random_tweet():
	sources = ["Twitter Web App"]*16
	sources.extend(["Twitter for Android"]*36)
	sources.extend(["Twitter for iPad"]*2)
	sources.extend(["Twitter for iPhone"]*40)
	sources.extend(["Unnoficial"]*5)

	rules = [{"id": 1373083929650028544, "tag": "dog"}] * 42
	rules.extend([{"id": 1378012949659784171, "tag": "cat"}] * 56)
	rules.extend([{"id": 1372213365145524519, "tag": "both 'cat' and 'dog'"}] * 2)

	source = random.choice(sources)
	return {
		"_id": "".join([str(random.randint(0,9)) for i in range(19)]),
		"data": {
			"author_id": "".join([str(random.randint(0,9)) for i in range(19)]),
			"created_at": datetime.now(),
			"id": "".join([str(random.randint(0,9)) for i in range(19)]),
			"public_metrics": {
				"like_count": random.randint(0,1000),
				"quote_count": random.randint(0,1000),
				"reply_count": random.randint(0,1000),
				"retweet_count": random.randint(0,1000)
			},
			"source": {
				"original": source,
				"parsed": source
			},
			"text": random_text(20,140)
		},
		"includes": {
			"users": [{
				"id": "1330969732820987907",
				"name": "random - " +random_text(10,20),
				"public_metrics": {
					"followers_count": random.randint(0,1000),
					"following_count": random.randint(0,10000),
					"listed_count": random.randint(0,100),
					"tweet_count": random.randint(0,100000)
				},
				"username": "random "+random_text(10,20)
			}]
		},
		"matching_rules": [ random.choice(rules)]
	}

def log(s):
	"""prepends a timestamp to a string and print it to stdout"""
	ts = datetime.now().strftime("[%Y-%m-%d %H:%M:%S]")
	print("%s %s" % (ts, s))

client = pymongo.MongoClient('mongodb://localhost:27017')
db = client.twitterStream
while True:
	time.sleep(random.uniform(0,1))
	r = db.tweets.insert_one(get_random_tweet())
	log("Tweet generated")
